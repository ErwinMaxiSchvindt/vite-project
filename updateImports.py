#https://regex101.com/r/2NgeCs/1
import re
import subprocess
import os

finish = False
count = 1
while not finish and count <= 10:
    count = count + 1
    a = subprocess.Popen('npm run build', shell=True, stdout=subprocess.PIPE, stderr=subprocess.PIPE, text=True)
    out = str(a.stdout.read())
    err = str(a.stderr.read())

    aux = re.findall(r"Error: Could not resolve \'(.*?)\' from (.*?)\n", err)

    # check different load error -> https://regex101.com/r/xvN782/1
    if len(aux) == 0:
        aux = re.findall(r"Could not load (.*?) \(imported by (.*?)\):", err)

    # If we don't have error, the build was ok
    if len(err) == 0 or len(aux) == 0:
        finish = True
        print("Finish, the build is ok")
        exit(0)

    # Check the result [(component, file)]
    if len(aux[0]):
        importLine = aux[0][0]
        fileToUpdate = aux[0][1]

        path = os.path.split(fileToUpdate)
        fileToCheck = importLine.replace("./", path[0]+"/")

        if os.path.isfile(fileToCheck + '.vue'):
            print("Update " + importLine + " in " + fileToUpdate)

            with open(fileToUpdate, 'r+') as f:
                text = f.read()
                # Added ' ' to replace with context
                text = re.sub("'"+importLine+"'", "'"+importLine+".vue'", text)
                f.seek(0)
                f.write(text)
                f.truncate()
        else:
            print(fileToCheck + " is not a vue component")









